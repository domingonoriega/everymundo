import React from "react";
import classNames from "classnames";
import moment from "moment";
import { TripTypeSelector, AirportTextField, DatePicker } from "./index";

const BookingMaskForm = props => {
  return (
    <form>
      <div className="row">
        <div className="col-md-12 trip-types">
          <TripTypeSelector
            isRoundTrip={props.isRoundTrip}
            onSetIsRoundTrip={props.onSetIsRoundTrip}
          />
        </div>
      </div>
      <div className="row">
        <div className="col-md-3 text-input">
          <label>From</label>
          <AirportTextField
            value={props.departureAirport}
            onChange={event => props.onSetDepartureAirport(event.target.value)}
            placeHolder="Departure"
          />
        </div>
        <div className="col-md-3 text-input">
          <label>To</label>
          <AirportTextField
            value={props.arrivalAirport}
            onChange={event => props.onSetArrivalAirport(event.target.value)}
            placeHolder="Arrival"
          />
        </div>
        <div className="col-md-2 text-input">
          <label>Departure</label>
          <DatePicker
            onChange={(field, value) => props.onSetDepartureDate(value)}
            value={props.departureDate}
          />
        </div>
        <div
          className={classNames("col-md-2 text-input", {
            show: props.isRoundTrip,
            hide: !props.isRoundTrip
          })}
        >
          <label>Return</label>
          <DatePicker
            onChange={(field, value) => props.onSetReturnDate(value)}
            value={props.returnDate}
            startDate={props.departureDate}
            placeholder={moment(props.departureDate)
              .add(7, "days")
              .format("DD-MMM-YYYY")}
          />
        </div>
        <div className="col-md-2 text-input">
          <button
            type="button"
            className="btn btn-info btn-search"
            onClick={props.onSearchClick}
          >
            Search
          </button>
        </div>
      </div>
    </form>
  );
};
export default BookingMaskForm;
