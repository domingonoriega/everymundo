import React from "react";

const AirportTextField = props => (
  <input
    className="form-control"
    type="text"
    value={props.value}
    onChange={props.onChange}
    placeholder={props.placeHolder}
  />
);

export default AirportTextField;
