import React from "react";
import classNames from "classnames";

const TripTypeSelector = props => {
  return (
    <ul className="nav nav-tabs" role="tablist">
      <li className="nav-item">
        <a
          className={classNames("nav-link", {
            active: props.isRoundTrip
          })}
          data-toggle="tab"
          role="tab"
          onClick={() => props.onSetIsRoundTrip(true)}
        >
          Round Trip
        </a>
      </li>
      <li className="nav-item">
        <a
          className={classNames("nav-link", {
            active: !props.isRoundTrip
          })}
          data-toggle="tab"
          role="tab"
          onClick={() => props.onSetIsRoundTrip(false)}
        >
          One way
        </a>
      </li>
    </ul>
  );
};
export default TripTypeSelector;
