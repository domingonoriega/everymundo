import React, { Component } from "react";
import BookingMask from "./BookingMask";

class App extends Component {
  render() {
    return (
      <div className="App">
        <BookingMask />
      </div>
    );
  }
}

export default App;
