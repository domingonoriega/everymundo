import React, { Component } from "react";
import moment from "moment";
import { BookingMaskForm } from "../components";

class BookingMask extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      isRoundTrip: true,
      departureDate: moment(),
      departureAirport: "",
      arrivalAirport: ""
    };
  }

  setIsRoundTrip = value => {
    this.setState({ isRoundTrip: value });
  };

  setDepartureDate = value => {
    this.setState({ departureDate: value });
  };

  setReturnDate = value => {
    this.setState({ returnDate: value });
  };

  setDepartureAirport = value => {
    this.setState({ departureAirport: value });
  };

  setArrivalAirport = value => {
    this.setState({ arrivalAirport: value });
  };

  validateForm = () => {
    // Report Errors
    if (
      this.state.departureAirport === "" ||
      this.state.arrivalAirport === ""
    ) {
      return false;
    }

    if (!this.state.departureDate) {
      return false;
    }

    if (this.state.isRoundTrip && !this.state.returnDate) {
      return false;
    }

    return true;
  };

  createBookUrl = () => {
    return `https://www.swiss.com/us/en/Book/${
      this.state.isRoundTrip ? "Round" : "OneWay"
    }/${this.state.departureAirport}-${
      this.state.arrivalAirport
    }/from-${this.state.departureDate.format("YYYY-MM-DD")}/${
      this.state.isRoundTrip
        ? "to-" + this.state.returnDate.format("YYYY-MM-DD") + "/"
        : ""
    }al-LX/sidqu26`;
  };

  onSearchClick = () => {
    if (this.validateForm()) {
      alert(this.createBookUrl());
    } else {
      alert("Missing Fields");
    }
  };

  render() {
    return (
      <div className="container">
        <div className="booking-mask card">
          <h2 className="booking-mask-title">EVERYMUNDO</h2>
          <BookingMaskForm
            isRoundTrip={this.state.isRoundTrip}
            departureDate={this.state.departureDate}
            returnDate={this.state.returnDate}
            departureAirport={this.state.departureAirport}
            arrivalAirport={this.state.arrivalAirport}
            onSetIsRoundTrip={this.setIsRoundTrip}
            onSetDepartureDate={this.setDepartureDate}
            onSetReturnDate={this.setReturnDate}
            onSetDepartureAirport={this.setDepartureAirport}
            onSetArrivalAirport={this.setArrivalAirport}
            onSearchClick={this.onSearchClick}
          />
        </div>
      </div>
    );
  }
}
export default BookingMask;
